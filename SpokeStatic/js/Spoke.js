var $ENV = {
		'lng':{
			'loc':'',
			'loc_def':'',
			'loc_num':'',
			'lng':'',
			'lng_def':'',
			'lng_num':''
		},
		'loc':'US',
		'host':'',
		'hostip':'159.89.124.39',
		'cdn':'https://z3n4u6x4.stackpathcdn.com/',
		'cdn_origin':'https://cdn.dialognoc.com/',
		'cache':'',
		'api':'',
		'mode':'',
		'dev':'n',
		'min':'.min'
	},
	$PGE = {},
	$USR = {},
	$FLX = {},
	$IDX = {},
	$ICO = {
		'bell':'<svg viewBox="0 0 99 99"><path d="M89 85H9c-6 0-8-5-5-9l13-17V38c0-14 11-26 26-29V6a6 6 0 0113 0v3c15 3 26 15 26 29v21l13 17c4 5 1 9-6 9zm-53 3a14 14 0 0027 0H36z"/></svg>',
		'arrow':'<svg viewBox="0 0 99 99"><path d="M40.5 3.5C27-7 9.2 8.5 21 20.7c2.4 2.4 24 25 24 28.6s-20.6 25.5-24 29c-13 14 8.6 27.2 19.5 17.2C44.2 92 79.5 60.3 82 58c4-3.6 6.6-11 0-17.2-2.6-2.2-39.2-35.5-41.5-37.3z"/></svg>',
		'burger':'<svg viewBox="0 0 99 99"><path d="M0 40h99v20H0V40zM96 4H3C1 4 0 5 0 7v17h99V7c0-2-1-3-3-3zm3 71H0v17c0 2 1 3 3 3h93c2 0 3-1 3-3V75z"/></svg>',
		'delete':'<svg viewBox="0 0 98 98"><path d="M81.8 27H15.2c-8 0-8-12.8-1-12.8 0 0 12.7 2 15.7-10.8 0-3 2.8-4 4.8-4h27.4c2 0 4 1 5 4C69 17 82.7 14.2 82.7 14.2c7 0 7 12.7-1 12.7zm-45 59.7c0 5-8 5-8 0v-43s8-5 8 0v43zm15.6 0c0 5-7.8 5-7.8 0v-43s7.8-5 7.8 0v43zm15.7 0c0 5-7.7 5-7.7 0v-43s7.8-5 7.8 0v43zm11-54H18c-4.8 0-7.7 3-6.7 8l6.8 50c1 4 5 7.8 9 7.8h43c4 0 9-3 9-8l6.7-49.8c0-5-3-8-6.8-8z"/></svg>',
		'download':'<svg viewBox="0 0 99 99"><path d="M73 65h-4L55 78V41c0-2-1-3-3-3h-5c-2 0-3 1-3 3v37L30 65h-4l-4 4v4l26 25h4l25-25v-4l-4-4z"/><path opacity=".7" d="M63 0C50 0 39 8 34 19 15 13 4 28 4 41c0 11 7 21 19 22h11l1-2V51l-1-2h-8c-13 0-13-24 8-16 4 1 9 0 14-9 3-6 9-10 16-10 24 1 23 33 2 35l-2 2v9l1 2h8c12 0 23-16 23-30C96 16 84 0 63 0z"/></svg>',
		'error':'<svg viewBox="0 0 98 98"><path d="M49 0C22 0 0 22 0 49s22 49 49 49 49-22 49-49S76 0 49 0zm0 93.7c-24.6 0-44.7-20-44.7-44.7 0-24.6 20-44.7 44.7-44.7s44.7 20 44.7 44.7c0 24.6-20 44.7-44.7 44.7zm0-84.4C27 9.3 9.3 27 9.3 49 9.3 71 27 88.7 49 88.7S88.7 71 88.7 49 71 9.3 49 9.3zm6.8 9.2L54 59H44l-2-40.5h13.8zm-6.8 61c-4 0-7.2-3.4-7.2-7.2 0-4 3.2-7.3 7.2-7.3s7.2 3.3 7.2 7.3c0 3.8-3.3 7.2-7.2 7.2z"/></svg>',
		'gear':'<svg viewBox="0 0 100 100"><path d="M90 50c0-6 4-11 10-15-1-4-2-7-4-10-6 2-12-1-16-5s-6-10-4-16c-3-2-7-3-10-4-4 6-10 10-16 10S38 6 34 0c-4 1-7 2-10 4 2 6 0 12-4 16s-10 7-16 5c-2 4-3 7-4 10 6 4 10 9 10 15S6 62 0 66c1 4 2 7 4 10 6-2 12 0 16 4s6 10 4 16c3 2 6 3 10 4 3-6 10-10 16-10s12 4 16 10c4-1 7-2 10-4-2-6 0-12 4-16s10-7 16-5c2-3 3-6 4-10-6-4-10-9-10-15zM50 72c-12 0-22-10-22-22s10-22 22-22 22 10 22 22-10 22-22 22z"/></svg>',
		'home':'<svg viewBox="0 0 99 99"><path d="M50 0L0 45l5 6 5-4v40c0 7 5 12 11 12h17V59h23v40h17c6 0 11-5 11-12V47l5 4 5-6L50 0z"/></svg>',
		'link':'<svg viewBox="0 0 99 99"><path d="M43 75l-7 7C24 94 4 76 17 64l19-19c5-5 17-7 21 5l10-9a26 26 0 00-41-5L8 54a26 26 0 000 37c10 11 27 11 37 0l14-14c-6 1-13 0-16-2zm31-12c-10 10-32 13-42-5l10-9c4 12 16 10 22 5l18-18c12-12-6-31-19-19l-6 7c-6-2-11-3-17-2L54 8C64-3 81-2 91 8s11 26 0 36L74 63z"/></svg>',
		'magnify':'<svg viewBox="0 0 99 99"><path d="M98 41c0 24-25 53-61 36L19 95C8 104-6 91 4 81l18-18C10 46 14 3 57 1c22-1 41 17 41 40zM57 64c29 0 29-45 0-45s-29 45 0 45z"/></svg>',
		'mappin':'<svg viewBox="0 0 99 99"><path d="M49.5 0a34.5 34.5 0 00-28 54.5l28 44.5 28-44.5A34.5 34.5 0 0049.5 0zm0 15a16.5 16.5 0 110 33 16.5 16.5 0 010-33z"/></svg>',
		'ok':'<svg viewBox="0 0 98 98"><path d="M49 0C22 0 0 22 0 49s22 49 49 49 49-22 49-49S76 0 49 0zm0 93.7c-24.6 0-44.7-20-44.7-44.7S24.3 4.3 49 4.3s44.7 20 44.7 44.7-20 44.7-44.7 44.7zm0-84.4C27 9.3 9.3 27 9.3 49S27 88.7 49 88.7 88.7 71 88.7 49C88.7 27 71 9.3 49 9.3zm1.7 64.3h-16L24 51H38l5 11.2 16.6-37.8H74L50.6 73.6z"/></svg>',
		'play':'<svg viewBox="0 0 99 99"><path d="M93 45L14 1c-4-2-7 1-7 4v89c0 4 4 6 7 4l79-44c3-2 3-7 0-9z"/></svg>',
		'plus':'<svg viewBox="0 0 50 50"><path d="M50 18H32V0H18v18H0v14h18v18h14V32h18"/></svg>',
		'save':'<svg viewBox="0 0 98 98"><path d="M91.3 0H6.7C3 0 0 3 0 6.7V71c0 2.7 1 5.2 3 7l19.8 20V61h53v37h15.5c3.7 0 6.7-3 6.7-6.7V6.7C98 3 95 0 91.3 0zm-46 98H30.8V68H45v30z"/></svg>',
		'spoke':'<svg viewBox="0 0 99 99"><path d="M47.2 49.6c-2.3.7-5.3-1.2-6.4-2-13.3-9.7-19.5-7.5-26.7-9.2C7.7 37 3.3 31.7 5.6 26.1c2.3-5.5 7.5-6.7 12-4.6 7.5 3.3 7.9 14.1 24.9 21.6-.2 3 1.8 5.7 4.7 6.5zm8.6 6.3c17 7.5 17.4 18.3 24.9 21.6 4.5 2 9.7.9 12-4.6 2.3-5.6-2-10.8-8.5-12.3-7.2-1.7-13.4.5-26.7-9.2-1.1-.8-4-2.7-6.4-2 3 .8 5 3.5 4.7 6.5zm-7.7-8c-1.8-1.7-1.6-5.2-1.4-6.5 1.7-16.4-3.3-20.8-5.5-27.8C39.3 7.3 41.7 1 47.6.1c6-.8 9.5 3.1 10 8 1 8.2-8.2 14-6.2 32.4a6.4 6.4 0 00-3.3 7.3zm-1.2 10.6c2 18.4-7.1 24.2-6.3 32.3.6 5 4.2 8.9 10 8.1 6-.8 8.4-7.2 6.5-13.5-2.2-7-7.2-11.4-5.5-27.8.2-1.3.4-4.8-1.4-6.4.8 2.9-.6 6-3.3 7.3zM50 47.7c.6-2.3 3.8-3.9 5-4.4C70 36.6 71.3 30 76.3 24.7c4.5-4.8 11.2-6 14.9-1.2 3.6 4.7 2 9.8-2 12.7C82.6 41 73 36 58 47a6.4 6.4 0 00-8 .8zm-9.8 4.4c-15 11-24.5 5.9-31 10.7-4.1 3-5.7 8-2 12.7 3.6 4.8 10.3 3.6 14.9-1.2 5-5.4 6.2-11.9 21.2-18.6 1.3-.5 4.4-2 5-4.4a6.4 6.4 0 01-8 .8z"/></svg>',
		'switch':'<svg viewBox="0 0 99 73"><path opacity=".4" d="M36 73h29c44-2 47-70-1-73H36c42 2 47 68 0 73z"/><circle class="SwitchSVG C4fl" cx="31.5" cy="36.5" r="31.5"/></svg>',translate:'<svg viewBox="0 0 99 99"><path d="M59 35V2H0v60h38V50l-7-7-13 11-3-5 12-10c-5-8-8-17-8-17l6-1s2 7 7 13c4-5 5-10 6-14H13v-7h14V9h7v4h13v7h-3c0 6-4 13-9 19l3 4v-5c0-2 1-3 3-3h18zm14 35l-3-15-5 15h8zM42 37l-2 2v58h59V37H42zm35 49l-2-10H64l-2 10h-9l11-37h11l11 37h-9z"/></svg>',
		'translate':'<svg viewBox="0 0 99 99"><path d="M59 35V2H0v60h38V50l-7-7-13 11-3-5 12-10c-5-8-8-17-8-17l6-1s2 7 7 13c4-5 5-10 6-14H13v-7h14V9h7v4h13v7h-3c0 6-4 13-9 19l3 4v-5c0-2 1-3 3-3h18zm14 35l-3-15-5 15h8zM42 37l-2 2v58h59V37H42zm35 49l-2-10H64l-2 10h-9l11-37h11l11 37h-9z"/></svg>',
		'user':'<svg viewBox="0 0 99 99"><path opacity=".2" d="M50 86c-29 0-37-30-9-30h19c30 3 20 30-10 30z"/><path d="M50 0C22 0 0 22 0 50s22 49 50 49c27 0 49-22 49-49S75 0 50 0zm17 32c0 23-34 23-34 0-1-28 34-29 34 0zM50 86c-29 0-37-30-9-30h19c30 3 20 30-10 30z"/></svg>'
	},
	$PRM = {},
	$LOC = {},
	$LNG = {},
	VPortW,
	VPortH,
	resizeTimeout;

if(isObj($APP) === false) var $APP = {};

(function(){
	/*! Polyfills */
	if(window.Element && !Element.prototype.closest){
		Element.prototype.closest =
		function(s){
			var matches = (this.document || this.ownerDocument).querySelectorAll(s),
				i,
				el = this;
			do {
				i = matches.length;
				while (--i >= 0 && matches.item(i) !== el) {};
			} while ((i < 0) && (el = el.parentElement));
			return el;
		};
	}
	
	VPortW = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	VPortH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	
	if(typeof CacheBust != 'undefined' && CacheBust) $ENV['cache'] = '?cache='+CacheBust;
	if(typeof DevMode != 'undefined' && DevMode === 'y'){
		$ENV['dev'] = 'y';
		$ENV['min'] = '';
		$ENV['cdn'] = $ENV['cdn_origin'];
	}

	init_App(function(){
		init_Environment(function(){
			if(typeof $ADM_tkn !== 'undefined' && $ADM_tkn){
				load_Scripts(['js/Admin'], function(){
					//App is in admin mode
					init_Admin($APP_nme);
				});
			}else{
				init_Page('', function(){
					setup_Page(function(){
						load_Page('init', function(){
							//$.when(init_User()).done(function(){
							//$USR Initialized
							//});
						});
					});
				});	
			}
		});
	});
})();

function init_App(callback){
	if(typeof $APP_nme !== 'undefined' && $APP_nme){
		if(typeof $APP !== 'undefined' && isObj($APP) && $APP['nme'] && $APP['nme'] === $APP_nme){
			//App is already loaded
			callback();
		}else{
			load_JSON($ENV['cdn']+'json/'+$APP_nme+'/app.json'+$ENV['cache'], function(d){
				if(d){
					var dta = JSON.parse(d),
						loc_tot = 0,
						lng_tot = 0;
				
					for(var loc in dta['loclang']){
						if(loc_tot === 0) $ENV['lng']['loc_def'] = loc;
						for(var lng in dta['loclang'][loc]){
							if(loc_tot === 0 && lng_tot === 0) $ENV['lng']['lng_def'] = lng;
							lng_tot++;
						}
						loc_tot++;
					}
				
					$ENV['lng']['loc'] = $ENV['lng']['loc_def'];
					$ENV['lng']['lng'] = $ENV['lng']['lng_def'];
					$ENV['lng']['loc_num'] = loc_tot;
					$ENV['lng']['lng_num'] = lng_tot;

					if(dta && dta['aid']){
						for(var k in dta) {
							$APP[k] = dta[k];
						}
						callback();
					}else{
						Error('init_App', 'Real JSON Fail');
					}
				}else{
					Error('init_App', 'Parse JSON Fail');
				}
			});
		}
	}else{
		Error('init_App', 'Unidentified App');
	}
}

function init_Environment(callback){
	$ENV['mode'] = 'site';
	$ENV['host'] = $APP['host'];
	if($ENV['dev'] !== 'y' && $APP['cdn']) $ENV['cdn'] = $APP['cdn'];
	
	var hnm = window.location.hostname,
		dfl = ($ENV['dev']) ? '?dev=y' : '';
		
	if('https://'+hnm+'/' === $APP['host']){
		//served server-side through conventional host & htaccess
		$ENV['mode'] = 'site';
		$ENV['host'] = $APP['host'];
	}else{
		//handle links internally w/o refreshing the page
		$ENV['mode'] = 'app';
		if(hnm) $ENV['host'] = 'https://'+hnm+'/';
	}
	
	$ENV['api'] = $ENV['host']+'spoke-api.php'+dfl;
	callback();
}

function init_Page(xid, callback){
	var $Q = {},
		NV = 'Content';

	if(xid){
		$Q['xid'] = xid;
	}else{
		//query by filename
		var p = prepFLE(window.location.pathname);
		if(p['fld'] === 'xid' && p['fle']){
			$Q['xid'] = p['fle'].replace('.html', '');
		}else{
			if(p['fld'] && p['fle'] && $APP['loclang']){
				for(var loc in $APP['loclang']){
					for(var lng in $APP['loclang'][loc]){
						var v = $APP['loclang'][loc][lng];
						if(v['l'] && v['l'] === p['fld']){
							NV = 'Locations';
							loc = -1;
							break;
						}else if(v['p'] && v['p'] === p['fld']){
							NV = 'Products';
							loc = -1;
							break;
						}
					}
				}
			}
			$Q['fle'] = p['fle'];
			$Q['fld'] = p['fld'];
		}
	}

	var fNV = ($Q['xid']) ? $Q['xid'].charAt(0) : '';
	if(fNV === 'L'){
		NV = 'Locations';
	}else if(fNV === 'P'){
		NV = 'Products';
	}

	if(isObj($Q) > 0){
		get_Data('get_Content', NV, $Q, function(d){
			if(d && d['msg']){
				if(d['msg'] === 'OK' && d['data']){
					$PGE = d['data'];
				}else{
					$PGE = '404';
				}
			}else{
				$PGE = '500';
			}
			callback();	
		});
	}else{
		Error('data_Page', 'No Identifiers');
	}
}

function setup_Page(callback){
	var loc = $PGE['loc'],
		lng = $PGE['lng'],
		mn = getElem('main'),
		css_app = ($APP['css'] && $APP['css'][loc] && $APP['css'][loc][lng]) ? prepHTML($APP['css'][loc][lng], $PGE) : '',
		css_pge = ($PGE['pge_head'] && $PGE['pge_head']['css']) ? prepHTML($PGE['pge_head']['css'], $PGE) : '';
		
	//Insert Page Specific CSS
	if(css_app || css_pge){
		var c = document.createElement('style');
		c.type = 'text/css';
		c.innerText = css_app+css_pge;
		document.body.prepend(c);
	}

	//Check and create header, main & footer elements			
	setup_Page_section('header');

	if(!document.body.contains(mn)){
		mn = document.createElement('main');
		setAttr(mn, {"id":"main"});
		document.body.appendChild(mn);
	}
	
	setup_Page_section('footer');

	var $S = [];
	if($PGE['pge_head'] && $PGE['pge_head']['dna'] && $PGE['pge_head']['dna'].charAt(0) == '1'){
		$S.push('js/lib/jquery-3.4.1');
	}
	//$S.push('js/lang/'+loc+'-'+lng);
	$S.push('js/apps/'+$APP_nme);
	load_Scripts($S, function(){
		callback();
	});
}

function setup_Page_section(el){
	var e = getElem(el),
		a = (el === 'footer') ? 'foot' : 'head',
		d = (el === 'footer') ? 2 : 1,
		sts = (isObj($APP[a]) > 0 && $PGE['pge_head'] && $PGE['pge_head']['dna'] && $PGE['pge_head']['dna'].charAt(d) == '1') ? 'on' : 'off';

	if(sts === 'on'){
		if(!document.body.contains(e)){
			e = document.createElement(el);
			setAttr(e, {"id":el});
			document.body.appendChild(e);
		}
	}else{
		RemoveDiv(e);
	}

	if(sts === 'on'){
		//Insert Common Header & Footer
		var loclng = $PGE['loc']+'-'+$PGE['lng'];
		if($APP[a]['html']) e.innerHTML = prepHTML($APP[a]['html'], $PGE);
		if($APP[a]['elem'] && loclng){
			for(var i = 0; i < isObj($APP[a]['elem'][loclng]); i++){
				var v = $APP[a]['elem'][loclng][i];
				if(v['html']){
					var t = (v['tar']) ? getElem(v['tar']) : e;
					t.innerHTML += prepHTML(v['html'], $PGE);
				}
			}
		}	
	}
}

function load_Page(mde, callback){
	console.log($PGE);

	//Insert Page Specific HTML
	if($PGE['pge_head'] && $PGE['pge_head']['html']){
		document.body.innerHTML += prepHTML($PGE['pge_head']['html'], '');
	}
	
	if($PGE['xid'] === 'NotFound'){
		var f = document.createElement('div');
		setAttr(f, {"class":"shim40"});
		f.innerHTML = '<h1 class="center">404 Not Found</h1>';
		getElem('main').appendChild(f);
		callback();
	}else if($PGE){
		load_Sections('main', $PGE);

		var $S = [];
		if($PGE['pge_head'] && $PGE['pge_head']['dna']){
			var dna = $PGE['pge_head']['dna'];
			if(dna.charAt(3) === '1') $S.push('js/lib/SpokeUI');
			if(dna.charAt(4) === '1') $S.push('js/lib/Calendar');
			if(dna.charAt(5) === '1') $S.push('js/lib/Forms');
			if(dna.charAt(6) === '1') $S.push('js/lib/svgMap');
			if(dna.charAt(7) === '1') $S.push('js/lib/Maps');
		}
		load_Scripts($S, function(){
			//Run Page Specific js
			var ps = getElem('PageScript');
			if(ps) ps.parentNode.removeChild(ps);
			if($PGE['pge_head'] && $PGE['pge_head']['js']){
				var js = prepHTML($PGE['pge_head']['js'].replace(/\\'/g, "'"), $PGE),
					js_blk = document.createElement('script'),
					js_txt = document.createTextNode(js);
					
				setAttr(js_blk, {"id":"PageScript"});
				js_blk.appendChild(js_txt);
				document.body.appendChild(js_blk);
			}

			if(mde === 'init' && typeof SpokeInitialized === 'function') SpokeInitialized();
			callback();
			
			if(dna && dna.charAt(7) === '1') init_libMaps();
			find_VID();
			Sizer('');
		});
	}
}
function load_Sections(tar, dta){
	var e = (tar === 'ifr') ? getElem('PageLayoutFrame').contentWindow.getElem('main') : getElem(tar);
	if(e && dta && dta['pge_sections']){
		//empty it
		while(e.firstChild){
			e.removeChild(e.firstChild);
		}
		//fill it up
		var numsect = dta['pge_sections'].length;
		for(var i = 0; i < numsect; i++){
			var s = document.createElement('section'),
				$S = dta['pge_sections'][i],
				a = {};

			if($S['id']) a['id'] = $S['id'];
			if($S['class']) a['class'] = $S['class'];
			if(isObj(a) > 0) setAttr(s, a);

			if($S['html']) s.innerHTML = prepHTML($S['html'], dta);
			e.appendChild(s);
		}
	}
}

function nav_Page(xid, url){
	//this is ugly
	var $Q = {};
	if(xid){
		$Q['xid'] = xid;
	}else if(url){
		$Q['fld'] = '',
		$Q['fle'] = '';
		url = url.replace($ENV['host'], '');
		if(url.indexOf('/') > 0){
			var u = url.split('/');
			if(u[0] != '' && u[1] != ''){
				$Q['fld'] = u[0].replace('/', '');
				$Q['fle'] = u[1].replace('.html', '');
			}else if(u[0]){
				$Q['fld'] = u[0].replace('/', '');
			}
			
		}else{
			$Q['fle'] = (url.indexOf('.html') > 0) ? url.replace('.html', '') : url.replace('/', '');
		}
	}

	get_Data('get_Content', 'Content', $Q, function(d){
		if(d && d['data']){
			$PGE = d['data'];
			if($PGE['loc'] != d['data']['loc'] || $PGE['lng'] != d['data']['lng']){
				setup_Page(function(){
					load_Page('dyn', function(){
						//console.log('lang switch');
					});
				});	
			}else{
				load_Page('dyn', function(){
					//console.log('no lang switch');
				});
			}
		}
	});
}

function init_User(){
	var def = new $.Deferred();
	if($USR['usr']){
		if($APP['dna'][0] !== 'X'){
			if($.cookie('usr') && typeof $.cookie('usr') !== 'undefined'){
				$USR['usr'] = $.cookie('usr');
			}else if(window.localStorage['usr'] && window.localStorage['usr'].length === 32){
				$USR['usr'] = window.localStorage['usr'];
			}
			if($USR['usr']){
				//next
			}
		}else{
			def.resolve();
		}
	}else{
		def.resolve();
	}
	return def.promise();
}

function SpokeResize(){
	clearTimeout(resizeTimeout);
	resizeTimeout = setTimeout(function(){
		VPortW = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		VPortH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		var mb = getElem('ModalBox');
		if(mb) SizeSpokeModal(mb.getAttribute('data-w'), mb.getAttribute('data-h'));
		Sizer();
	}, 250);
}

/*! Data Querying */
function get_Data(act, tbl, $Q, callback){
	var cached = 'n';
	if(act === 'get_Content' && tbl !== 'Media'){
		if(typeof $EDT != 'undefined' && $EDT && $EDT['xid'] === $Q['xid']){
			cached = 'y';
			callback({'msg':'OK', 'data':$EDT});
		}
	}
	if(cached === 'n'){
		var xhr = new XMLHttpRequest(),
			qry = 'act='+act+'&app='+$APP['aid'];
			
		if(tbl) qry += '&tbl='+tbl;
		if(typeof $ADM != 'undefined' && $ADM && $ADM['ssn']) qry += '&ssn='+$ADM['ssn'];
		if(isObj($Q) > 0){

			if(act.indexOf('query_') > -1 || act.indexOf('manage_') > -1){
				qry += '&dta='+JSON.stringify($Q).replace('&', '%26');
			}else{
				for(var k in $Q){
					if($Q[k] && $Q[k] != 'undefined') qry += '&'+k+'='+$Q[k];
				}
			}
		}
		xhr.open('POST', $ENV['api'], true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.onload = function(){
			var d = (xhr.responseText) ? JSON.parse(xhr.responseText) : {};
			callback(d);
		};
		xhr.send(qry);
	}
}

function db(act, tbl, dta, callback){
	var xhr = new XMLHttpRequest(),
		fmd = new FormData();

	fmd.append('act', act);
	fmd.append('app', $APP['aid']);
	if(tbl) fmd.append('tbl', tbl);
	if(dta){
		if(act.indexOf('query_') > -1 || act.indexOf('manage_') > -1){
			fmd.append('dta', JSON.stringify(dta));
		}else{
			for(var k in dta){
				fmd.append(k, dta[k]);
			}
		}
	}
	if(typeof $ADM != 'undefined' && $ADM && $ADM['ssn']) fmd.append('ssn', $ADM['ssn']);

	xhr.onload = () => {
		if(xhr.status >= 200 && xhr.status < 300){
			var d = (xhr.responseText) ? JSON.parse(xhr.responseText) : {};
			callback(d);
		}
	};
	xhr.open('POST', $ENV['api'], true);
	xhr.send(fmd);
}

function upload(tar, typ, callback){
	var $F = getElem(tar).files[0],
		r = {'err':''};

	if($F && $F['size'] > 0 && $F['name']){
		if($F['size'] <= 5242880){
			var	hastyp = 'n',
				typlen = (typ) ? typ.length : 0;

			for(var i = 0; i < typlen; i++){
				if(typ[i] === $F['type']){
					hastyp = $F['type'];
					break;
				}
			}
			if(hastyp !== 'n'){
				
				var	xhr = new XMLHttpRequest(),
					fmd = new FormData();

				fmd.append('act', 'upload_Media');
				fmd.append('app', $APP['nme']);
				fmd.append('file', $F);
				if(typeof $ADM['ssn'] != 'undefined' && $ADM['ssn']){
					console.log($ADM['ssn']);
					fmd.append('ssn', $ADM['ssn']);
				}

				xhr.onload = () => {
					if(xhr.status >= 200 && xhr.status < 300){
						r = (xhr.responseText) ? JSON.parse(xhr.responseText) : {};
						callback(r);
					}
				};
				xhr.open('POST', $ENV['api'], true);
				xhr.send(fmd);

			}else{
				r['err'] = 'Invalid File Type';
				callback(r);	
			}
		}else{
			r['err'] = 'Larger than 5MB';
			callback(r);	
		}
	}else{
		r['err'] = 'No File';
		callback(r);
	}
}

function load_CSS(p){
	if(p){
		var n = p.replace('/', '');
		if(getElem(n)){
			//CSS already loaded
		}else{
			var e = document.createElement('link');
			e.rel = 'stylesheet';
			e.type = 'text/css';
			e.href = $ENV['cdn']+'css/'+p+$ENV['min']+'.css'+$ENV['cache'];
			e.id = n;
			document.head.appendChild(e);
		}
	}
}
function load_Scripts(sc, callback){
	var prom = sc.length,
		prog = 0;

	if(prom === 0){
		callback();
	}else{
		var dvm = ($ENV['dev'] === 'y') ? 'dev=y' : '',
				djn = ($ENV['cache']) ? '&' : '?';

		for(var i = 0; i < prom; i++){
			var url = $ENV['cdn']+sc[i]+$ENV['min']+'.js'+$ENV['cache']+djn+dvm,
				exists = 'n';

			document.body.querySelectorAll('script').forEach(function(nd){
				if(nd.getAttribute('src') === url){
					console.log(url+' Exists');
					exists = 'y';
				}else{
					console.log(url+' Does Not Exist');
				}
			});

			if(exists === 'n'){
				var s = document.createElement('script'),
					id = '';

				s.type = 'text/javascript';
				if(sc[i].indexOf('js/lang') >= 0){
					id = 'SpokeAdminLang';
				}else if(sc[i].indexOf('js/lang') >= 0){
					id = 'SpokeLang';
				}
				if(id) setAttr(s, {"id":id});
				s.src = url;
				s.onload = function(){
					prog++;
					if(prog === prom) callback();
				};
				document.body.appendChild(s);	
			}else{
				prog++;
				if(prog === prom) callback();
			}
		}
	}
}

function load_LanguageLib(lng, mde, callback){
	if(mde === 'sys' && isObj($LNG[mde]) > 0){
		callback();
	}else if(isObj($LNG[mde]) > 0 && $LNG[mde+'_lng'] && $LNG[mde+'_lng'] === lng){
		callback();
	}else{
		var fle = '';
		if(mde === 'sys'){
			fle = 'lib/Languages/SpokeLanguages';
		}else if(mde === 'app' && lng){
			fle = $APP['nme']+'/lng/'+lng;
		}else if(mde === 'ui' && lng){
			fle = 'lib/Languages/SpokeUI/'+lng;
		}
		if(fle){
			load_JSON($ENV['cdn']+'json/'+fle+$ENV['min']+'.json'+$ENV['cache'], function(c){
				if(c){
					$C = JSON.parse(c);
					if(!$LNG[mde]) $LNG[mde] = {};
					if(mde === 'sys'){
						for(var k in $C){
							if($C[k]['f']){
								//inactive language
							}else{
								$LNG[mde][k] = $C[k];
							}
						}
					}else{
						$LNG[mde][lng] = $C;
						$LNG[mde+'_lng'] = lng;
					}
					callback();
				}
			});
		}
	}
}

function load_Countries(lng, callback){
	if(isObj($LOC) > 0){
		trans_Countries(lng, function(){
			callback();
		});
	}else{
		load_JSON($ENV['cdn']+'json/lib/Countries/SpokeCountries'+$ENV['min']+'.json'+$ENV['cache'], function(c){
			if(c){
				$LOC = JSON.parse(c);
				trans_Countries(lng, function(){
					callback();
				});
			}
		});
	}
}
function trans_Countries(lng, callback){
	if(lng && lng !== 'en'){
		var fk = Object.keys($LOC)[0];
		if($LOC[fk][lng]){
			//translation already applied
			callback();
		}else{
			load_JSON($ENV['cdn']+'json/lib/Languages/CountryNames/'+lng+$ENV['min']+'.json'+$ENV['cache'], function(t){
				if(t){
					var $T = JSON.parse(t),
						i = 0;

					for(var k in $T){
						if(i === 0){
							$LOC[k][lng] = ($T[k]) ? $T[k] : $LOC[k]['n'];
						}else{
							if($T[k]) $LOC[k][lng] = $T[k];
						}
						i++;
					}
					callback();
				}
			});
		}
	}else{
		callback();
	}
}
function load_Country(loc, lng, callback){
	if(!$LOC[loc]) $LOC[loc] = {};
	if(isObj($LOC[loc]['json']) > 0){
		callback();
	}else{
		load_JSON($ENV['cdn']+'json/lib/Countries/'+loc+$ENV['min']+'.json'+$ENV['cache'], function(c){
			if(c){
				$LOC[loc]['json'] = JSON.parse(c);
				callback();
			}
		});
	}
}

function GetSpecialFolder(mde, loc, lng){
	var r = '';
	if(isObj($APP['loclang']) > 1){
		loc = (loc in $APP['loclang']) ? loc : $ENV['lng']['loc_def'];
		lng = (lng in $APP['loclang'][loc]) ? lng : $ENV['lng']['lng_def'];
	}
	if(mde === 'Locations' && $APP['loclang'][loc][lng]['l']){
		r = $APP['loclang'][loc][lng]['l'];
	}else if(mde === 'Products' && $APP['loclang'][loc][lng]['p']){
		r = $APP['loclang'][loc][lng]['p'];
	}
	return r;
}

function load_Flex(n, callback){
	if(isObj($IDX[n]) > 0 && $IDX['loc'] === $ENV['lng']['loc'] && $IDX['lng'] === $ENV['lng']['lng']){
		if(callback) callback();
	}else{
		var fle = n.toLowerCase();
		load_JSON($ENV['cdn']+'json/'+$APP_nme+'/'+fle+'.json'+$ENV['cache'], function(d){
			if(d){
				d = JSON.parse(d);
				$IDX[n] = conv_Flex('IDX', n, d.idx);
				$FLX[n] = conv_Flex('FLX', n, d.flx);
			}
			if(callback) callback();
		});
	}
}

function conv_Flex(typ, mde, $d){
	var loc = $ENV['lng']['loc'],
		lng = $ENV['lng']['lng'],
		$r = {'loc':loc, 'lng':lng, 'fld':{}};

	if(isObj($d) > 0){
		for(var k in $d){
			var v = $d[k],
				lbl = (isObj(v['lbl']) > 0 && v['lbl'][loc] && v['lbl'][loc][lng]) ? v['lbl'][loc][lng] : '',
				key = (typ === 'IDX') ? 'idx'+k : k;

			$r['fld'][key] = {'typ':v['typ'], 'lbl':lbl};
		}
	}
	return $r;
}
function load_JSON(file, callback){
	var j = new XMLHttpRequest();
	j.overrideMimeType('application/json');
	j.open('GET', file, true);
	j.onreadystatechange = function(){
		if(j.readyState === 4 && j.status === 200){
			callback(j.responseText);
		}
	}
	j.send(null);
}

/*! Content Processing */
function Sizer(tar){
	var s = (tar) ? getElem(tar).querySelectorAll('.Sizer') : document.querySelectorAll('.Sizer');
	if(s){
		var slen = s.length;
		for(var i = 0; i < slen; i++){
			Sizer_proc(s[i]);
		}
	}
}
function find_VID(){
	var e = document.getElementsByClassName('VidThumb'),
		len = e.length;

	for(var i = 0; i < len; i++){
	   init_VidThumb(e.item(i));
	}
}
function init_VidThumb(el){
	var d = el.getAttribute('vid-dna'),
		$d = prepVID(d),
		ins = '<div class="VidThumbMenu">';
		
	if($d['src'] === 'mp4'){
		ins += '<div class="VidThumbRes Btn32 C1bk C2bk_hov C0fl" vid-dna="'+d+'">'+$ICO['play']+'</div>'+
		'<div class="VidThumbResMenu hide"></div>';
	}else{
		ins += '<div class="Btn32 C1bk C2bk_hov C0fl" vid-dna="'+d+'">'+$ICO['play']+'</div>'+
			'<a href="'+$d['url']+'" class="Btn32 C1bk C2bk_hov C0fl" target="_blank">'+$ICO['link']+'</a>';
	}
		
	ins += '</div>';
	el.innerHTML = ins;
}

function Sizer_proc(e){
	if(e){
		var dna = e.getAttribute('img-dna'),
			$dna = (dna && dna.indexOf('|') >= 0) ? dna.split('|') : {},
			suf = ($dna[0] && $dna[0].indexOf('-') !== -1) ? '-' : '_',		//remove after MO update
			orig_res = 0,
			max_w = 150,
			res = 150,
			newfile = '',
			w = e.clientWidth;

		if($dna && $dna[1]){
			if($dna && $dna[2]){
				max_w = $dna[2];
			}else{
				if(e.getAttribute('data-w')){
					max_w = parseInt(e.getAttribute('data-w'));
				}
			}
			
			if(w >= 150 && max_w >= 300) res = 300;
			if(w >= 300 && max_w >= 600) res = 600;
			if(w >= 600 && max_w >= 1200) res = 1200;
			if(w >= 1200 && max_w >= 1600) res = 1600;

			if($dna){
				newfile = $ENV['cdn']+'img/'+$APP['nme']+'/'+$dna[0]+suf+res+'.'+$dna[1];
			}

			if(newfile && w > orig_res){
				if(orig_res < res){
					var objImg = new Image();
					objImg.src = newfile;
					objImg.onload = function(){
						if(e.tagName === 'IMG'){
							e.src = newfile;
						}else{
							e.style.backgroundImage = 'url('+newfile+')';
						}
					}
				}
			}
		}
	}
	return true;
}
function Sizer_TwoBox(ow, oh, iw, ih){
	var R = {'w':ow, 'h':Math.ceil(ow / iw * ih), 'x':0, 'y':0};
	if(R.h < oh){
		R.w = oh / R.h * R.w;
		R.h = oh;	
	}
	if(R.w - ow != 0) R.x = 0 - (R.w - ow) / 2;
	if(R.h - oh != 0) R.y = 0 - (R.h - oh) / 2;
	return R;
}
function prepHTML(h, dta){
	if(h.indexOf('##') > 0){
		//environment
		var $R1 = {
			'##CDN##':$ENV['cdn'],
			'##IMG##':$ENV['cdn']+'img/'+$APP['nme']+'/',
			'##DOC##':$ENV['cdn']+'doc/'+$APP['nme']+'/',
			'##VID##':$ENV['cdn']+'vid/'+$APP['nme']+'/',
			'##URL##':$ENV['host']
		};
		h = replaceObjMap(h, $R1);

		//icons
		if(h.indexOf('##ICO_') > 0){
			var rgx = h.match(/[##ICO_]+([a-z\_\-])+[##]+/g);
			for(var i = 0; i < isObj(rgx); i++){
				var ico = rgx[i].replace(/##/g, '').replace('ICO_', '');
				if(ico && ico in $ICO) h = h.replace(rgx[i], $ICO[ico]);
			}
		}
		if(dta){
			var $R2 = {
				'##title##':dta['title'],
				'##caption##':dta['caption'],
				'##thumb_min##':'',
				'##thumb_dna##':''
			};
			//dates
			$R2['##date_created##'] = prepDate(dta['created'], 'dte');
			$R2['##date_pub_start##'] = prepDate(dta['pub_start'], 'dte');
			$R2['##date_pub_start_long##'] = prepDate(dta['pub_start'], 'dte_long');
			if(dta['modified'] && dta['modified'] > 0) $R2['##date_modified##'] = prepDate(dta['modified'], 'dte');

			//media
			if(dta['thumb']){
				var thm = prepIMG(dta['thumb']);
				if(thm){
					$R2['##thumb_min##'] = $ENV['cdn']+thm['min'];
					$R2['##thumb_dna##'] = dta['thumb'];
				}
			}
			//indexes
			if(h.indexOf('##idx_') > 0){
				var rgx = h.match(/[##idx_]+([0-9])+[##]+/g);
				for(var i = 0; i < isObj(rgx); i++){
					var fl = rgx[i].replace(/##/g, '').replace('idx_', '');
					$R2['##idx_'+fl+'##'] = (fl && dta['idx'+fl]) ? dta['idx'+fl] : '';
				}
			}
			//flex object
			if(h.indexOf('##flex_') > 0){
				var rgx = h.match(/[##flex_]+([a-z\_\-])+[##]+/g);
				for(var i = 0; i < isObj(rgx); i++){
					var fl = rgx[i].replace(/##/g, '').replace('flex_', '');
					$R2['##flex_'+fl+'##'] = (fl && dta['flex'] && dta['flex'][fl]) ? dta['flex'][fl] : '';
				}
			}
			h = replaceObjMap(h, $R2);
		}
	}
	return (h && h != 'undefined') ? h : '';
}
function prepURL(d){
	var $U = {
			'mde':'',
			'url':'',
			'json':'',
			'data-json':'',
			'a':{
				'full':'',
				'href':'',
				'target':'',
				'class':'',
				'classname':''
			},
			'div':{
				'full':'',
				'data-url':'',
				'class':'',
				'classname':'Link'
			}
		};

	if(d && d['xid']){
		
		if(isObj(d['flex']) > 0 && d['flex']['redir_type'] && d['flex']['redir_type'] != ''){
			$U['mde'] = d['flex']['redir_type'];
		}
		if(isObj(d['flex']) > 0 && d['flex']['redir_url'] && d['flex']['redir_url'] != ''){
			$U['url'] = d['flex']['redir_url'];
		}else{
			var fld = (d['folder']) ? d['folder']+'/' : '',
				fle = (d['filename']) ? d['filename']+'.html' : '';
				
			if(fld || fle) $U['url'] = $ENV['host']+fld+fle;	
		}
		
		$U['a']['href'] = ' href="'+$U['url']+'"';
		$U['div']['data-url'] = ' data-url="'+$U['url']+'"';

		if($U['mde'] === 'blank'){
			$U['a']['target'] = ' target="_blank"';
			$U['div']['classname'] = 'NewWindow';
		}else if($U['mde'] === 'pop'){
			$U['a']['classname'] = 'LinkPop';
			$U['div']['classname'] = 'LinkPop';
		}
	
		if($U['a']['classname']) $U['a']['class'] = ' class="'+$U['a']['classname']+'"';
		if($U['div']['classname']) $U['div']['class'] = ' class="'+$U['div']['classname']+'"';
		$U['a']['full'] = $U['a']['href']+$U['a']['class']+$U['a']['target']+$U['data-json'];
		$U['div']['full'] = $U['div']['class']+$U['div']['data-url']+$U['data-json'];
	}
	return $U;
}
function prepFLE(d){
	var fle = '', fld = '';
	if(d){
		if(d.substring(0,4) === 'http') d = d.replace(/^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//, '');
		if(d && d.charAt(0) === '/') d = d.substr(1);
		if(d){
			if(d.indexOf('/') > 0){
				if(d.slice(-1) === '/'){
					fld = d.slice(0, -1); 
				}else{
					var df = d.split('/');
					fle = df[1];
					fld = df[0];	
				}
			}else{
				fle = d;
			}
		}
	}
	return {'fle':fle,'fld':fld};
}
function prepIMG(dna, mde){
	var d;
	if(dna && dna.indexOf('|') >= 0){
		var dn = dna.split('|'),
			maxres = 150;
			
		if(dn && dn[0] && dn[0] != 'undefined'){
			if(dn[2] >= 1600){
				maxres = 1600;
			}else if(dn[2] >= 1200){
				maxres = 1200;
			}else if(dn[2] >= 600){
				maxres = 600;
			}else if(dn[2] >= 300){
				maxres = 300;
			}
			
			var	suf = (dn[0].indexOf('-') !== -1) ? '-' : '_',		//remove after MO update
				res = (mde === 'max') ? maxres : 150,
				clr = (dn[4] && (dn[4].length === 3 || dn[4].length === 6)) ? '#'+dn[4] : '';
			
			d = {
				'code':dn[0],
				'typ':dn[1],
				'w':dn[2],
				'h':dn[3],
				'color':clr,
				'maxres':maxres,
				'min':'img/'+$APP['nme']+'/'+dn[0]+suf+'150.'+dn[1],
				'max':'img/'+$APP['nme']+'/'+dn[0]+suf+res+'.'+dn[1],
				'attr':'data-w="'+dn[2]+'" data-h="'+dn[3]+'"'
			};
		}else{
			d = false;
		}
	}else{
		d = false;
	}
	return d;
}
function prepVID(s, tar){
	var $R = {
			'src':'',
			'id':'',
			'url':'',
			'embed':''
		};
	
	if(s && s.indexOf('|') >= 0){
		$s = s.split('|');
		$R['id'] = $s[0];
		$R['src'] = $s[1];
		if($s[1] === 'youtube'){
			$R['url'] = 'https://www.youtube.com/watch?v='+$R['id'];
			$R['embed'] = 'https://www.youtube.com/embed/'+$R['id']+'?autoplay=1';
		}else if($s[1] === 'vimeo'){
			$R['url'] = 'https://vimeo.com/'+$R['id'];
			$R['embed'] = 'https://vimeo.com/'+$R['id'];
		}else if($s[1] === 'mp4'){
			$R['url'] = $ENV['cdn']+'vid/'+$APP['nme']+'/'+$s[0]+'-'+$s[3]+'.mp4';
		}
	}
	
	if(tar){
		var el = getElem(tar);
		if(el){
			var ins = '<div class="VidWrapper">';
			if($s[1] === 'mp4'){
				ins += '<video class="VidPlayer" autoplay><source src="'+$R['url']+'" type="video/mp4"></video>';
			}else{
				ins += '<iframe class="VidPlayer" src="'+$R['embed']+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			}
			ins += '</div>';
			el.innerHTML = ins;
		}
	}else{
		return $R;
	}
}
function prepNUM(num){
	num = ''+num;
	var ths_rgx = /(\d+)(\d{3})/,
		num_str = num.replace(/[^0-9.]/g, ''),
		x1 = '',
		x2 = '',
		lng = ($PGE['lng']) ? $PGE['lng'] : $ENV['lng']['lng'],
		ths = ',',
		dec = '.';

	if($LNG['ui'] && lng && $LNG['ui'][lng]){
		ths = $LNG['ui'][lng]['num']['ths'];
		dec = $LNG['ui'][lng]['num']['dec'];
	}
		
	if(num_str.indexOf('.') >= 0){
		var x = num_str.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? dec + x[1] : '';
	}else{
		x1 = num_str;
	}
    while(ths_rgx.test(x1)){
        x1 = x1.replace(ths_rgx, '$1'+ths+'$2');
    }
	
	return x1 + x2;
}
function prepCUR(sum, sym){
	var r = '$9',
		lng = ($PGE['lng']) ? $PGE['lng'] : $ENV['lng']['lng'];

	sum = (Math.round(parseFloat(sum) * 100) / 100).toFixed(2);
	sum = prepNUM(sum);

	if($LNG['ui'] && lng && $LNG['ui'][lng] && $LNG['ui'][lng]['cur']){
		r = $LNG['ui'][lng]['num']['cur'];
	}

	r = r.replace('9', sum);
	r = r.replace('$', sym);
	return r;
}
function prepPERC(num){
	var r = '9%',
		lng = ($PGE['lng']) ? $PGE['lng'] : $ENV['lng']['lng'];

	num = prepNUM(num);
	if($LNG['ui'] && lng && $LNG['ui'][lng] && $LNG['ui'][lng]['prc']){
		r = $LNG['ui'][lng]['num']['prc'];
	}
	r = r.replace('9', num);
	return r;
}
function prepDate(tme, mde){
	var now = getNow(),
		frm = '',
		loc = ($PGE & $PGE['loc']) ? $PGE['loc'] : $ENV['lng']['loc'],
		lng = ($PGE & $PGE['lng']) ? $PGE['lng'] : $ENV['lng']['lng'],
		mdt = '##Y##-##n##-##d##',
		mtm = '##H##:##i##:##s##',
		$F = {
			"dte":"##j##-##m##-##Y##",
			"dte_long":"##j## ##M## ##Y##",
			"months":{},
			"ord":{},
			"tme":"##G##:##i##",
			"tme_sec":"##G##:##i##:##s##",
			"tme_mde":"24",
			"tme_am":"##t## AM",
			"tme_pm":"##t## PM"
		};

	if(lng === 'en'){
		$F['tme_mde'] = 12;
		$F['tme'] = '##g##:##i## ##a##';
	}
	if($LNG['ui'] && $LNG['ui'][lng]){
		if($LNG['ui'][lng]['months']) $F['months'] = $LNG['ui'][lng]['months'];
		if($LNG['ui'][lng]['ord']) $F['ord'] = $LNG['ui'][lng]['ord'];
	}
	if(tme === 'now') tme = now;
	tme = parseInt(tme);

	if($LOC[loc] && $LOC[loc]['json'] && $LOC[loc]['json']['dte']){
		var $D = $LOC[loc]['json']['dte'];
		for(var k in $D){
			$F[k] = $D[k];
		}
	}
	
	if(tme && tme > 0){
		if(['dte', 'tme', 'dte_long', 'dte_long'].indexOf(mde) +1){	
			frm = $F[mde];
		}else if(mde === 'sql'){
			frm = mdt;
		}else if(mde === 'timestamp'){
			frm = mdt+' '+mtm;
		}else if(mde === 'utc'){
			frm = mdt+'T'+mtm+'Z';
		}else if(mde === 'utc_tme'){
			frm = '##H##:##i##';
		}else if(mde === 'best'){
			frm = ((now - tme) <= 79200) ? $F['tme'] : $F['dte'];
		}else if(mde === 'day'){
			frm = '##N##';
		}
		
		var d = new Date(0);
		d.setUTCSeconds(tme);
		
		var	hr24 = d.getHours(),
			hr12 = d.getHours(),
			ap = 'am',
			yr4 = d.getFullYear(),
			yr2 = yr4.toString().substr(2,2),
			mo = d.getMonth() + 1,
			dy = d.getDate(),
			dow = d.getDay(),
			ordsuff = '',
			monthname = mo;

		if($F['months'] && $F['months'][mo]){
			if(isObj($F['months'][mo]) > 0){
				monthname = $F['months'][mo]['n'];
			}else{
				monthname = $F['months'][mo];
			}
		}

		if(dow == 0) dow = 1;
		
		if(isObj($F['ord']) > 0){
			ordsuff = (dy in $F['ord']) ? $F['ord'][dy] : $F['ord'][0];
		}
		
		if(hr12 > 12){
			hr12 -= 12;
			ap = 'pm';
		}else if(hr12 === 12){
			ap = 'pm';
		}

		var r = {
			'##d##': ('0'+dy).slice(-2),
			'##j##': dy,
			'##N##': dow,
			'##m##': mo,
			'##M##': monthname,
			'##n##': ('0'+mo).slice(-2),
			'##Y##': yr4,
			'##y##': yr2,
			'##g##': hr12,
			'##G##': hr24,
			'##h##': ('0'+hr12).slice(-2),
			'##H##': ('0'+hr24).slice(-2),
			'##i##': ('0'+d.getMinutes()).slice(-2),
			'##s##': ('0'+d.getSeconds()).slice(-2),
			'##a##': ap,
			'##A##': ap.toUpperCase(),
			'##Q##': ordsuff
		};
		return (frm) ? replaceObjMap(frm, r) : r;
	}else{
		return '--';
	}
}

/*! SpokeModules */
function SpokeSearch_init(tar, mde, placeholder){
	var e = getElem(tar);
	if(e){
		var ac = (mde === 'autoc') ? ' autocomplete="off"' : '',
			ins = '<input type="text" id="'+tar+'_fld" class="SpokeSearchFld C0bk_grd"'+ac+' placeholder="'+placeholder+'" />'+
			'<div id="'+tar+'_btn" class="SpokeSearchBtn C1bk C2bk_hov">'+$ICO['magnify']+'</div>';

		if(mde === 'autoc') ins += '<div id="'+tar+'_autoc" class="SpokeSearchAutoC C0bk_grd C2br hide"></div>';

		e.innerHTML = ins;
		e.addEventListener('keyup', f => {
			var e = f.target,
				fnc = e.parentNode.getAttribute('data-fnc');
				
			fnc = (fnc && typeof window[fnc] == 'function') ? fnc : '';

			if(f.keyCode === 13){
				if(fnc) window[fnc]('search', e.value);
			}else if(f.keyCode === 27){
				e.value = '';
				SpokeSearch_hideauto(e);
			}else{
				var c = e.parentNode.querySelector('.SpokeSearchAutoC');
				if(c){
					if(e.value.length >= 2){
						if(fnc) window[fnc]('suggest', e.value);
						c.innerHTML = '<div class="center shim10"><div class="Ico24 C2fl o6"><div class="spin">'+$ICO['spoke']+'</div></div></div>';
						c.classList.remove('hide');
					}else{
						c.classList.add('hide');
					}
				}
			}
		});
		getElem(tar+'_fld').focus();
		getElem(tar+'_btn').addEventListener('pointerup', f => {
			var e = f.target,
				fld = e.parentNode.querySelector('.SpokeSearchFld'),
				fnc = e.parentNode.getAttribute('data-fnc');

			if(fnc && fld && fld.value) window[fnc]('search', fld.value);
		});
	}
}

function SpokeSearch_hideauto(e){
	var c = e.parentNode.querySelector('.SpokeSearchAutoC');
	if(c) c.classList.add('hide');
}

function Pagination_init(div, pge, lim, tot, mde){
	var e = getElem(div),
		ins = '';

	pge = parseInt(pge);
	tot = parseInt(tot);

	if(e){
		if(tot > 0){
			var num_pages = Math.ceil(tot / lim),
				end_range = (pge * lim > tot) ? tot : pge * lim,
				backcolor = '0',
				prv = pge - 1,
				nxt = pge + 1,
				fld = '',
				fle = '',
				btn_color = 'C1bk C2bk_hov C0fl',
				prev_clss = '',
				next_clss = '',
				link_clss = 'C1bk C2bk_hov C0',
				here_clss = 'C2bk C0';

			if(e.hasAttribute('data-backcolor')){
				backcolor = e.getAttribute('data-backcolor');
			}
			if($ENV['mode'] !== 'app'){
				fld = ($PGE && $PGE['folder']) ? $PGE['folder'] : '',
				fle = ($PGE && $PGE['filename']) ? $PGE['filename'] : '';
			}
			if(backcolor == '1'){
				btn_color = 'C0bk C2bk_hov C1fl';
				link_clss = 'C0bk C2bk_hov C1';
				here_clss = 'C2bk C1';
			}

			prev_clss = 'rot180 '+btn_color;
			next_clss = btn_color;

			if(pge > 1){
				prev_clss += ' Btn24 C2bk_hov';
				if($ENV['mode'] === 'app') prev_clss += ' Pag__Prev';
			}else{
				prev_clss += ' Ico24 o2';
			}

			if(pge < num_pages){
				next_clss += ' Btn24 C2bk_hov';
				if($ENV['mode'] === 'app') next_clss += ' Pag__Next';
			}else{
				next_clss += ' Ico24 o2';
			}

			ins += '<div class="Pag__Label">'+prepNUM((pge * lim) - lim + 1)+'-'+prepNUM(end_range)+' of '+prepNUM(tot)+'</div>';

			if($ENV['mode'] === 'app' || prv < 1){
				ins += '<div class="'+prev_clss+'" data-pge="'+prv+'">'+$ICO['arrow']+'</div>';
			}else{
				ins += '<a href="'+$ENV['host']+Pagination_link(fld, fle, prv)+'" class="'+prev_clss+'">'+$ICO['arrow']+'</a>';
			}

			if(VPortW < 800 || mde === 'select'){
				ins += '<select class="Pag__Page C1br">';
				for(var j = 1; j <= num_pages; j++){
					var sel = (j == pge) ? ' SELECTED' : '';
					ins += '<option value="'+j+'"'+sel+'>'+prepNUM(j)+'</option>';
				}
				ins += '</select>';
			}else{
				var strt = 1,
					stop = num_pages,
					fico = '',
					lico = '';

				if(num_pages > 7){
					if(pge >= 4){
						strt = pge - 3;
						stop = pge + 3;
						fico = '<div class="Pag__Extended noselect">...</div>';
					}else{
						stop = 7;
						if(pge <= (num_pages - 3)) lico = '<div class="Pag__Extended noselect">...</div>';
					}
				}

				if(fico) ins += fico;
				for(var j = strt; j <= stop; j++){
					var act = (j === pge) ? here_clss+' Pag__Link--Inactive o9' : link_clss+' Pag__Link';
					if($ENV['mode'] === 'app'){
						ins += '<div class="Btn24 '+act+'" data-pge="'+j+'">'+prepNUM(j)+'</div>';
					}else{
						ins += '<a href="'+$ENV['host']+Pagination_link(fld, fle, j)+'" class="Btn24 '+act+'">'+prepNUM(j)+'</a>';
					}
				}
				if(lico) ins += lico;
			}

			if($ENV['mode'] === 'app' || nxt > num_pages){
				ins += '<div class="'+next_clss+'" data-pge="'+nxt+'">'+$ICO['arrow']+'</div>';
			}else{
				ins += '<a href="'+$ENV['host']+Pagination_link(fld, fle, nxt)+'" class="'+next_clss+'">'+$ICO['arrow']+'</a>';
			}
		}
		e.classList.add('Pag');
		e.innerHTML = ins;
	}
}
function Pagination_link(fld, fle, pge){
	var nfle = fle;
	if(nfle){
		//is file, append pagination
		nfle += '-pg'+pge+'.html'
	}else if(fld){
		//is folder, create pagination filename
		nfle = 'pg'+pge+'.html';
	}
	if(fld) fld = fld+'/';
	return fld+nfle;
}

/*! Misc UI */
function LoadingIcon(tar){
	var e = getElem(tar);
	if(e){
		var ins = '<div class="spin">'+$ICO['spoke']+'</div>';
		if(e.innerHTML) ins += '<div class="SpokeLoadingInitial">'+e.innerHTML+'</div>';
		e.innerHTML = ins;
	}
}

function LoadingIconDone(tar, doneico){
	var e = getElem(tar);
	if(e){
		var ins = '';
		if(doneico && doneico != 'undefined'){
			ins = doneico;
		}else if(e.querySelectorAll('.SpokeLoadingInitial')){
			var h = e.querySelectorAll('.SpokeLoadingInitial').innerHTML;
			if(h && h != 'undefined') ins = h;
		}
		e.innerHTML = ins;
	}
}
function OpenModal(w, h, menu){
	CloseModal();
	var ins = '<div id="ModalClose" class="BtnClose C0bk"><div class="C1fl C2fl_hov rot45">'+$ICO['plus']+'</div></div>';
	if(menu === 'y'){
		ins += '<div id="ModalMenu"></div><div id="ModalStage" class="StageWithMenu"></div>';
	}else{
		ins += '<div id="ModalStage"></div>';
	}

	var m = document.createElement('div'),
		c = document.createElement('div');
	
	setAttr(m, {"id":"Modal"});
	m.innerHTML = '<div id="ModalBox" class="C0bk C3 C2br" data-w="'+w+'" data-h="'+h+'">'+ins+'</div>';

	document.body.appendChild(m);
	setAttr(c, {"id":"ModalCover", "class":"C0bk_grd"});
	document.body.appendChild(c);

	SizeSpokeModal(w, h);
}
function SizeSpokeModal(w, h){
	var b = getElem('ModalBox'),
		pad = 5,
		max_w = VPortW - (pad * 2),
		max_h = VPortH - (pad * 2);
		
	if(w && w != 'undefined'){
		if((typeof w === 'string' || w instanceof String) && w.indexOf('%') !== -1){
			w = max_w * (w.replace('%', '') / 100);
		}else if(w === 'max' || w > max_w){
			w = max_w;
		}
	}else{
		w = 500;
	}
	if(h && h != 'undefined'){
		if((typeof h === 'string' || h instanceof String) && h.indexOf('%') !== -1){
			h = max_h * (h.replace('%', '') / 100);
		}else if(h === 'max' || h > max_h){
			h = max_h;
		}
	}else{
		h = 500;
	}
	
	b.style.width = w+'px';
	b.style.height = h+'px';
	b.style.marginLeft = '-'+((Math.round(w / 2 * 10) / 10) - pad)+'px';
	b.style.marginTop = '-'+((Math.round(h / 2 * 10) / 10) - pad)+'px';
}
function CloseModal(){
	RemoveDiv('ModalCover');
	RemoveDiv('Modal');
	//RemoveDiv('Draggable');
}
function CheckSpokeModal(){
	return (getElem('Modal').length) ? 'open' : 'closed';
}
function MediaPopup(mde, param){
	if(mde && param && param.indexOf('|') > 0){
		var p = param.split('|'),
			w = parseInt(p[2]),
			wid = w,
			h = parseInt(p[3]),
			clr = (p[4] && p[4] != '') ? p[4] : '0e0918';

		if(w > 0 && h > 0){
			var img_rat = w / h,
				max_w = VPortW - 30,
				max_h = VPortH - 30,
				scn_rat = max_w / max_h;
				
			if(max_w > w) max_w = w;
			if(max_h > h) max_h = h;
				
			if(scn_rat < 1){					//vertical screen
				if(scn_rat < img_rat){			//vertical bound
					wid = max_w;
					h = h * (w / max_w);
				}else{
					if(h > max_h){
						wid = w * (max_h / h);
						h = max_h;
					}
				}
			}else{								//horizontal screen
				if(scn_rat < img_rat){			//horizontal bound
					if(w > max_w){
						wid = max_w;
						h = h * (max_w / w);
					}
				}else{
					wid = w * (max_h / h);
					h = max_h;
				}
			}
			OpenModal(wid, h);

			var mb = getElem('ModalBox'),
				suf = (p[0].indexOf('-') !== -1) ? '-' : '_';		//remove after MO update

			md.style.backgroundColor = '#'+clr;
				
			if(mde === 'img'){
				md.style.backgroundImage = 'url('+$ENV['cdn']+'img/'+$APP['nme']+'/'+p[0]+suf+w+'.'+p[1]+')';
				mb.classList.add('ModalBox--Image');
			}else if(mde === 'vid'){
				var $V =  prepVID(param, 'ModalBox');
				mb.innerHTML += '<iframe width="100%" class="VideoPreview" src="'+$V['embed']+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			}
	
			setAttr(md, {'data-w':p[2], 'data-h':p[3]});
		}
	}
}
function NewWindow(url){
	if(url){
		var w = window.open();
		w.opener = null;
		w.location = url; 
	}
}

/*! Helper Utilities */
function isObj(o){
	if(o && typeof o === 'object' && o !== null && o !== undefined){
		var len = Object.keys(o).length;
		return (len > 0) ? len : 0;
	}else{
		return false;
	}
}
function isVal(s){
	return (s && s != 'undefined' && s != '') ? s : '';
}
function daysInMonth(month, year){
    return new Date(year, month, 0).getDate();
}
function getNow(){
	var n = new Date();
	return Math.round(n.getTime() / 1000);
}
function CreateFilename(v){
	v = v.trim();
	v = v.replace(/ |_|#/g, '-');
	v = v.replace(/'|’/g, '%27');
	return v;
}
function FileExist(url, callback){
    var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		console.log(xhr);
		if(xhr.status >= 200 && xhr.status < 300){
			callback(true);
		}else{
			callback(false);
		}
    }
    xhr.open('HEAD', url);
}
function UrlVars(){
    var v = [], h, p = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < numObj(p); i++){
        h = p[i].split('=');
        v.push(h[0]);
        v[h[0]] = h[1];
    }
    return v;
}
function Truncate(s, l){
	return (s && s.length > 0 && l > 0) ? s.length > l ? s.substring(0, l - 3)+ '...' : s : s;
}
function getElem(s){
	if(typeof s === 'string' || s instanceof String){
		s = (s.length && s[0] == '#') ? s.slice(1) : s;
		return document.getElementById(s);
	}else if(s instanceof Element){
		return s;
	}
	return null;
}
function getClosest(elem, selector){
	for( ; elem && elem !== document; elem = elem.parentNode ){
		if(elem.matches(selector)) return elem;
	}
	return null;
}
function EmptyDiv(id){
	var e = getElem(id);
	if(e && e.childNodes.length > 0){
		while(e.firstChild){
			e.removeChild(e.firstChild);
		}
	}
}
function RemoveDiv(id){
	var e = getElem(id);
	if(e) e.parentNode.removeChild(e);
}
function byClass(c, mde, cls){
	var e = document.querySelectorAll('.'+c),
		len = e.length;

	for(var i = 0; i < len; i++){
		if(mde === 'ins'){
			e[i].innerHTML = cls;
		}else if(mde === 'app'){
			e[i].innerHTML += cls;
		}else if(mde === 'add'){
			e[i].classList.add(cls);
		}else if(mde === 'rem'){
			e[i].classList.remove(cls);
		}else if(mde === 'del'){
			e[i].parentNode.removeChild(e[i]);
		}
	}
}
function isVisible(e){
    return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
}
function isDescendant(parent, child){
	var node = child.parentNode;
	while(node != null){
		if(node == parent) return true;
		node = node.parentNode;
	}
	return false;
}
function indexInParent(e){
    var c = e.parentNode.childNodes,
		num = 0;

    for(var i = 0; i < c.length; i++){
         if(c[i] == e) return num;
         if(c[i].nodeType == 1) num++;
    }
    return -1;
}
function replaceObjMap(str, mapObj){
    var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
    return str.replace(re, function(m){
        return mapObj[m];
    });
}
function setAttr(e, d){
	if(e){
		for(var k in d){
			e.setAttribute(k, d[k]);
		}
	}
}
function setVal(tar, val){
	tar = getElem(tar);
	if(tar){
		val = val || '';
		var e = (tar.nodeType) ? tar : getElem(tar);
		if(e) e.value = val;
	}
}
function getVal(tar){
	tar = getElem(tar);
	if(tar){
		var e = (tar.nodeType) ? tar : getElem(tar);
		return (e && e.value) ? e.value : '';
	}
}

/*! User Browser */
function detectLang(){
	var $R = {'loc':'', 'lng':''};
	if(isObj(navigator.languages) > 0){
		var r = navigator.languages[0].split('-');
		$R['loc'] = r[1];
		$R['lng'] = r[0];
	}
	return $R;
}
function setCookie(n, v){
    var d = new Date();
	d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    document.cookie = $APP['nme']+n+'='+(v || '')+'; expires='+d.toUTCString()+'; path=/';
}
function getCookie(n){
    var nEQ = $APP['nme']+n+'=', ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++){
        var c = ca[i];
        while(c.charAt(0)==' ') c = c.substring(1,c.length);
        if(c.indexOf(nEQ) == 0) return c.substring(nEQ.length,c.length);
    }
    return false;
}
function delCookie(n){   
    document.cookie = $APP['nme']+n+'=; Max-Age=-99999999;';  
}

/*! Lang UI */
function LocalizeDayMonth(v, mde, lng, abbr){
	var r = v,
		key = (mde === 'm') ? 'months' : 'days';

	if(lng && $LNG['ui'] && $LNG['ui'][lng] && $LNG['ui'][lng][key]){
		var $D = $LNG['ui'][lng][key];
		if(isObj($D[v]) > 0){
			r = $D[v]['n'];
			if(abbr === 'y' && $D[v]['a']) r = $D[v]['a'];
		}else if($D[v]){
			r = $D[v];
		}
	}
	return r;
}

/*! Error Handling */
function Error(nme, err){
	console.log(nme+' :: '+err);
}

/*! Global Events */
window.addEventListener('resize', SpokeResize);

document.addEventListener('change', f => {
	var e = f.target,
		id = e.getAttribute('id'),
		c = e.classList;
	
	if(c.contains('Pag__Page')){
		var fnc = getClosest(e, '.Pag').getAttribute('data-fnc');
		if(fnc && typeof window[fnc] == 'function') window[fnc](e.value);
	}
});

document.body.addEventListener('pointerup', f => {
	var e = f.target,
		id = e.getAttribute('id'),
		xid = e.getAttribute('data-xid'),
		ty = e.type;
	
	if(ty === 'a'){
		if($ENV['mode'] === 'app' && e.getAttribute('target') !== '_blank'){
			if(xid){
				nav_Page(xid, '');
			}else{
				nav_Page('', e.getAttribute('href'));
			}
		}
	}else{
		if(id === 'ModalClose'){
			CloseModal();
			var fnc = e.getAttribute('data-fnc');
			if(fnc && typeof window[fnc] == 'function'){
				window[fnc](p);
			} 
		}else{
			var c = e.classList;
			if(c.contains('Link')){
				var url = e.getAttribute('data-url');
				if($ENV['mode'] === 'app' && xid){
					nav_Page(xid, '');
				}else if($ENV['mode'] === 'app'){
					nav_Page('', url);
				}else{
					document.location.href = url;
				}
			}else if(c.contains('NewWindow')){
				NewWindow(e.getAttribute('data-url'));
			}else if(c.contains('LinkPop')){
				if(e.getAttribute('img-dna')){
					MediaPopup('img', e.getAttribute('img-dna'));
				}else if(e.getAttribute('vid-dna')){
					MediaPopup('vid', e.getAttribute('vid-dna'));
				}
			}else if(c.contains('DropDown')){
				if(isVisible(getClosest(e, 'ul'))){
					e.classList.remove('DropDownOn');
				}else{
					e.classList.add('DropDownOn');
				}
			}else if(c.contains('Pag__Link') || c.contains('Pag__Prev') || c.contains('Pag__Next')){
				var p = e.getAttribute('data-pge'),
					par = e.closest('.Pag'),
					fnc = par.getAttribute('data-fnc');
		
				if(fnc && typeof window[fnc] == 'function'){
					window[fnc](p);
				} 
			}
		}
	}
});